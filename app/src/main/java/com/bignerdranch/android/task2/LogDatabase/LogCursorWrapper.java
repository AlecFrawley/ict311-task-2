package com.bignerdranch.android.task2.LogDatabase;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.bignerdranch.android.task2.Log;
import com.bignerdranch.android.task2.LogDatabase.LogDbSchema.LogTable;
import com.bignerdranch.android.task2.LogDatabase.SettingsDbSchema.SettingsTable;

import java.util.Date;
import java.util.UUID;

public class LogCursorWrapper extends CursorWrapper {
    public LogCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Log getLog() {
        String uuidString = getString(getColumnIndex(LogTable.Cols.UUID));
        String title = getString(getColumnIndex(LogTable.Cols.TITLE));
        String duration = getString(getColumnIndex(LogTable.Cols.DURATION));
        String comment = getString(getColumnIndex(LogTable.Cols.COMMENT));
        long date = getLong(getColumnIndex(LogTable.Cols.DATE));
        Integer activitytype = getInt(getColumnIndex(LogTable.Cols.ACTIVITYTYPE));
        Log log = new Log(UUID.fromString(uuidString));

        log.setActivityType(activitytype);
        log.setComment(comment);
        log.setDuration(duration);
        log.setTitle(title);
        log.setDate(new Date(date));
        return log;
    }


}
