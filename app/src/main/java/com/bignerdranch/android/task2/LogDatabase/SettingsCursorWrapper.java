package com.bignerdranch.android.task2.LogDatabase;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.bignerdranch.android.task2.Log;
import com.bignerdranch.android.task2.LogDatabase.LogDbSchema.LogTable;
import com.bignerdranch.android.task2.LogDatabase.SettingsDbSchema.SettingsTable;

import java.util.Date;
import java.util.UUID;

public class SettingsCursorWrapper extends CursorWrapper {
    public SettingsCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Log getSettings() {
        String name = getString(getColumnIndex(SettingsTable.Cols.USERNAME));
        String id = getString(getColumnIndex(SettingsTable.Cols.ID));
        String email = getString(getColumnIndex(SettingsTable.Cols.EMAIL));
        Integer gender = getInt(getColumnIndex(SettingsTable.Cols.GENDER));
        String comment = getString(getColumnIndex(SettingsTable.Cols.COMMENT));
        Log settings = new Log();

        settings.setUserName(name);
        settings.setSComment(comment);
        settings.setID(id);
        settings.setEmail(email);
        settings.setGender(gender);
        return settings;
    }

}
