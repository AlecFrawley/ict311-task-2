package com.bignerdranch.android.task2.LogDatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bignerdranch.android.task2.LogDatabase.LogDbSchema.LogTable;
import com.bignerdranch.android.task2.LogDatabase.SettingsDbSchema.SettingsTable;

public class LogBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "logBase.db";

    public LogBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + LogTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                LogTable.Cols.UUID + ", " +
                LogTable.Cols.TITLE + ", " +
                LogTable.Cols.DATE + ", " +
                LogTable.Cols.DURATION + ", " +
                LogTable.Cols.COMMENT + ", " +
                LogTable.Cols.ACTIVITYTYPE + " " +
                ")"
        );

        db.execSQL("create table " + SettingsTable.SETTINGSNAME + "(" +
                " _id integer primary key autoincrement, " +
                SettingsTable.Cols.USERNAME + ", " +
                SettingsTable.Cols.ID + ", " +
                SettingsTable.Cols.EMAIL + ", " +
                SettingsTable.Cols.COMMENT + ", " +
                SettingsTable.Cols.GENDER + " " +
                ")"
        );
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
