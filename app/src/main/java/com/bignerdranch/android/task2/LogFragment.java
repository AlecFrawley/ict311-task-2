package com.bignerdranch.android.task2;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.util.UUID;

public class LogFragment extends Fragment {

    private static final String ARG_LOG_ID = "log_id";

    private Log mLog;
    private EditText mTitleField;
    private EditText mDurationField;
    private EditText mCommentField;
    private Button mDateButton;
    private Button mDeleteLog;
    private Button mSaveLog;
    private Spinner mActivityType;
    private ImageButton mPhotoButton;
    private ImageView mPhotoView;
    private File mPhotoFile;
    private static final int REQUEST_PHOTO = 2;

    public static LogFragment newInstance(UUID logId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_LOG_ID, logId);
        LogFragment fragment = new LogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID logId = (UUID) getArguments().getSerializable(ARG_LOG_ID);
        mLog = LogStorage.get(getActivity()).getLog(logId);
        mPhotoFile = LogStorage.get(getActivity()).getPhotoFile(mLog);
    }

    @Override
    public void onPause() {
        super.onPause();

        LogStorage.get(getActivity())
                .updateLog(mLog);
    }

    private void updatePhotoView() {
        if (mPhotoFile == null || !mPhotoFile.exists()) {
            mPhotoView.setImageDrawable(null);
        } else {
            Bitmap bitmap = PictureUtils.getScaledBitmap(
                    mPhotoFile.getPath(), getActivity());
            mPhotoView.setImageBitmap(bitmap);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_log, container, false);

        mTitleField = (EditText) v.findViewById(R.id.log_title);
        mTitleField.setText(mLog.getTitle());
        mDurationField = (EditText) v.findViewById(R.id.log_duration);
        mDurationField.setText(mLog.getDuration());
        mCommentField = (EditText) v.findViewById(R.id.log_comment);
        mCommentField.setText(mLog.getComment());
        mDeleteLog = (Button) v.findViewById(R.id.delete_log);
        mSaveLog = (Button) v.findViewById(R.id.save_log);
        mActivityType = (Spinner) v.findViewById(R.id.activity_type);


        mSaveLog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getActivity(), R.string.toast_save_log, Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        });

        mDeleteLog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UUID logId = mLog.getId();
                LogStorage.get(getActivity()).deleteLog(logId);

                Toast.makeText(getActivity(), R.string.toast_delete_log, Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        });

        mDurationField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {
                // This space intentionally left blank
            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mLog.setDuration(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // This one too
            }
        });

        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {
                // This space intentionally left blank
            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mLog.setTitle(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // This one too
            }
        });

        mCommentField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {
                // This space intentionally left blank
            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mLog.setComment(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                // This one too
            }
        });

        mActivityType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mLog.setActivityType(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mLog.setActivityType(0);
            }
        });


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.activity_type_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mDateButton = (Button) v.findViewById(R.id.log_date);
        mDateButton.setText(mLog.getDate().toString());
        mDateButton.setEnabled(false);

        mActivityType.setAdapter(adapter);
        mActivityType.setSelection(mLog.getActivityType());

        PackageManager packageManager = getActivity().getPackageManager();

        mPhotoButton = (ImageButton) v.findViewById(R.id.activity_camera);

        final Intent captureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        boolean canTakePhoto = mPhotoFile != null &&
                captureImage.resolveActivity(packageManager) != null;
        mPhotoButton.setEnabled(canTakePhoto);
        if (canTakePhoto) {
            Uri uri = Uri.fromFile(mPhotoFile);
            captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }
        mPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(captureImage, REQUEST_PHOTO);
            }
        });

        mPhotoView = (ImageView) v.findViewById(R.id.activity_photo);
        updatePhotoView();

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        } else if (requestCode == REQUEST_PHOTO) {
            updatePhotoView();
        }
    }
}

