package com.bignerdranch.android.task2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;



public class LogListFragment extends Fragment {
    private RecyclerView mLogRecyclerView;
    private LogAdapter mAdapter;
    private Button mNewLog;
    private Button mSettings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log_list, container, false);
        mNewLog = (Button) view.findViewById(R.id.new_log);
        mSettings = (Button) view.findViewById(R.id.settings_log);


        mLogRecyclerView = (RecyclerView) view
                .findViewById(R.id.log_recycler_view);
        mLogRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mNewLog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log log = new Log();
                LogStorage.get(getActivity()).addLogs(log);
                Intent intent = LogActivity
                        .newIntent(getActivity(), log.getId());
                startActivity(intent);
            }
        });

        mSettings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LogSettingsActivity.class));
            }
        });

        updateUI();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    private void updateUI() {
        LogStorage logStorage = LogStorage.get(getActivity());
        List<Log> logs = logStorage.getLogs();
        if (mAdapter == null) {
            mAdapter = new LogAdapter(logs);
            mLogRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setLogs(logs);
            mAdapter.notifyDataSetChanged();
        }
    }

    private class LogHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private TextView mTitleTextView;
        private TextView mDateTextView;
        private Log mLog;

        public LogHolder(View itemView) {
            super(itemView);
                itemView.setOnClickListener(this);
            mTitleTextView = (TextView)
                    itemView.findViewById(R.id.list_item_log_title_text_view);
            mDateTextView = (TextView)
                    itemView.findViewById(R.id.list_item_log_date_text_view);
        }

        public void bindLog(Log log) {
            mLog = log;
            mTitleTextView.setText(mLog.getTitle());
            mDateTextView.setText(mLog.getDate().toString());
        }

        @Override
        public void onClick(View v) {
            Intent intent = LogActivity.newIntent(getActivity(), mLog.getId());
            startActivity(intent);
        }

    }

    private class LogAdapter extends RecyclerView.Adapter<LogHolder> {
        private List<Log> mLogs;
        public LogAdapter(List<Log> logs) {
            mLogs = logs;
        }

        @Override
        public LogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(R.layout.list_item_log, parent, false);
            return new LogHolder(view);
        }

        @Override
        public void onBindViewHolder(LogHolder holder, int position) {
            Log log = mLogs.get(position);
            holder.bindLog(log);
        }

        @Override
        public int getItemCount() {
            return mLogs.size();
        }

        public void setLogs(List<Log> logs) {
            mLogs = logs;
        }

    }
}

