package com.bignerdranch.android.task2.LogDatabase;


public class LogDbSchema {
    public static final class LogTable {
        public static final String NAME = "logs";

        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DATE = "date";
            public static final String DURATION = "duration";
            public static final String COMMENT = "comment";
            public static final String ACTIVITYTYPE = "type";
        }
    }

}
