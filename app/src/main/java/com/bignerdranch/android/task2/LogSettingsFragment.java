package com.bignerdranch.android.task2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class LogSettingsFragment extends Fragment {
    private Button mSaveLog;
    private Spinner mGender;
    private Log mSettings;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSettings = SettingsStorage.get(getActivity()).getSettings();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.settings_fragment, container, false);
        mSaveLog = (Button) v.findViewById(R.id.save_log);
        mGender = (Spinner) v.findViewById(R.id.settings_gender);

        mSaveLog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getActivity(), R.string.toast_save_settings, Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        });

        //mGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            //@Override
            //public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //mSettings.setGender(position);
            //}

            //@Override
            //public void onNothingSelected(AdapterView<?> parent) {
                //mSettings.setActivityType(0);
            //}
        //});

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.gender_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mGender.setAdapter(adapter);
        //mGender.setSelection(mSettings.getActivityType());


        return v;
    }
}
