package com.bignerdranch.android.task2;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.UUID;

public class LogActivity extends SingleFragmentActivity {
    private static final String EXTRA_LOG_ID =
            "com.bignerdranch.android.task2.log_id";
    public static Intent newIntent(Context packageContext, UUID logId) {
        Intent intent = new Intent(packageContext, LogActivity.class);
        intent.putExtra(EXTRA_LOG_ID, logId);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        UUID logId = (UUID) getIntent()
                .getSerializableExtra(EXTRA_LOG_ID);
        return LogFragment.newInstance(logId);
    }
}
