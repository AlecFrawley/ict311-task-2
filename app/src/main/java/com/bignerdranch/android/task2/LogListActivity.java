package com.bignerdranch.android.task2;

import android.support.v4.app.Fragment;

public class LogListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new LogListFragment();
    }
}
