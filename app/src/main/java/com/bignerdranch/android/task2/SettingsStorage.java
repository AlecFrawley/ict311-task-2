package com.bignerdranch.android.task2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bignerdranch.android.task2.LogDatabase.LogBaseHelper;
import com.bignerdranch.android.task2.LogDatabase.SettingsCursorWrapper;
import com.bignerdranch.android.task2.LogDatabase.SettingsDbSchema.SettingsTable;

public class SettingsStorage {
    private static SettingsStorage sSettingsStorage;

    private Context mSContext;
    private SQLiteDatabase mSDatabase;

    public static SettingsStorage get(Context context) {
        if (sSettingsStorage == null) {
            sSettingsStorage = new SettingsStorage(context);
        }
        return sSettingsStorage;
    }
    private SettingsStorage(Context context) {
        mSContext = context.getApplicationContext();
        mSDatabase = new LogBaseHelper(mSContext)
                .getWritableDatabase();

    }

    public void addSettings(Log c) {
        ContentValues values = getContentValues(c);
        mSDatabase.insert(SettingsTable.SETTINGSNAME, null, values);
    }


    public Log getSettings() {
        SettingsCursorWrapper cursor = querySettings();
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getSettings();
        } finally {
            cursor.close();
        }
    }


    public void updateSettings(Log log) {
        ContentValues values = getContentValues(log);
        mSDatabase.update(SettingsTable.SETTINGSNAME, values,
                SettingsTable.Cols.USERNAME + " = ?",
                new String[] {
                });
    }

    private static ContentValues getContentValues(Log log) {
        ContentValues values = new ContentValues();
        values.put(SettingsTable.Cols.USERNAME, log.getUserName());
        values.put(SettingsTable.Cols.ID, log.getID());
        values.put(SettingsTable.Cols.EMAIL, log.getEmail());
        values.put(SettingsTable.Cols.GENDER, log.getGender());
        values.put(SettingsTable.Cols.COMMENT, log.getComment());
        return values;
    }

    private SettingsCursorWrapper querySettings() {
        Cursor cursor = mSDatabase.query(
                SettingsTable.SETTINGSNAME,
                null, // Columns - null selects all columns
                null, //
                null, //
                null, // groupBy
                null, // having
                null // orderBy
        );
        return new SettingsCursorWrapper(cursor);

    }

}
