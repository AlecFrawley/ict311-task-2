package com.bignerdranch.android.task2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.bignerdranch.android.task2.LogDatabase.LogBaseHelper;
import com.bignerdranch.android.task2.LogDatabase.LogCursorWrapper;
import com.bignerdranch.android.task2.LogDatabase.LogDbSchema.LogTable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LogStorage {
    private static LogStorage sLogStorage;

    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static LogStorage get(Context context) {
        if (sLogStorage == null) {
            sLogStorage = new LogStorage(context);
        }
        return sLogStorage;
    }
    private LogStorage(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new LogBaseHelper(mContext)
                .getWritableDatabase();

        for (int i = 0; i < 100; i++) {
            Log log = new Log();
            log.setTitle("Log #" + i);
        }
    }

    public void addLogs(Log c) {
        ContentValues values = getContentValues(c);
        mDatabase.insert(LogTable.NAME, null, values);
    }

    public void deleteLog(UUID logId) {
        String uuidString = logId.toString();
        mDatabase.delete(LogTable.NAME, LogTable.Cols.UUID + " = ?", new String[] {uuidString});
    }

    public List<Log> getLogs() {
        List<Log> logs = new ArrayList<>();
        LogCursorWrapper cursor = queryLogs(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                logs.add(cursor.getLog());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return logs;
    }

    public Log getLog(UUID id) {
        LogCursorWrapper cursor = queryLogs(
                LogTable.Cols.UUID + " = ?",
                new String[] { id.toString() }
        );
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getLog();
        } finally {
            cursor.close();
        }
    }

    public File getPhotoFile(Log log) {
        File externalFilesDir = mContext
                .getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (externalFilesDir == null) {
            return null;
        }
        return new File(externalFilesDir, log.getPhotoFilename());
    }

    public void updateLog(Log log) {
        String uuidString = log.getId().toString();
        ContentValues values = getContentValues(log);
        mDatabase.update(LogTable.NAME, values,
                LogTable.Cols.UUID + " = ?",
                new String[] { uuidString });
    }

    private static ContentValues getContentValues(Log log) {
        ContentValues values = new ContentValues();
        values.put(LogTable.Cols.UUID, log.getId().toString());
        values.put(LogTable.Cols.TITLE, log.getTitle());
        values.put(LogTable.Cols.DATE, log.getDate().getTime());
        values.put(LogTable.Cols.DURATION, log.getDuration());
        values.put(LogTable.Cols.COMMENT, log.getComment());
        values.put(LogTable.Cols.ACTIVITYTYPE, log.getActivityType());
        return values;
    }

    private LogCursorWrapper queryLogs(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                LogTable.NAME,
                null, // Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return new LogCursorWrapper(cursor);

    }

}
