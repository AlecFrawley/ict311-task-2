package com.bignerdranch.android.task2;

import java.util.Date;
import java.util.UUID;

public class Log {
    private UUID mId;
    private String mTitle;
    private Date mDate;
    private Integer mActivityType;
    private String mDuration;
    private String mComment;
    private String mSComment;
    private Integer mGender;
    private String mID;
    private String mEmail;
    private String mName;


    public Log() {
        // Generate unique identifier
        this(UUID.randomUUID());
    }

    public Log(UUID id) {
        mId = id;
        mDate = new Date();
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDuration() {
        return mDuration;
    }

    public void setDuration(String duration) {
        mDuration = duration;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public Integer getActivityType() {
        return mActivityType;
    }

    public void setActivityType (Integer activityType)  {
        mActivityType = activityType;
    }

    public String getPhotoFilename() {
        return "IMG_" + getId().toString() + ".jpg";
    }

    public String getSComment() {
        return mSComment;
    }

    public void setSComment(String comment) {
        mSComment = comment;
    }

    public Integer getGender() {
        return mGender;
    }

    public void setGender (Integer gender)  {
        mGender = gender;
    }

    public String getUserName() {
        return mName;
    }

    public void setUserName(String name) {
        mName = name;
    }

    public String getID() {
        return mID;
    }

    public void setID(String id) {
        mID = id;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

}
