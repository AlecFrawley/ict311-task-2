package com.bignerdranch.android.task2.LogDatabase;


public class SettingsDbSchema {

    public static final class SettingsTable {
        public static final String SETTINGSNAME = "settings";

        public static final class Cols {
            public static final String USERNAME = "Name";
            public static final String ID = "ID";
            public static final String EMAIL = "email";
            public static final String GENDER = "gender";
            public static final String COMMENT = "comment";
        }
    }

}
